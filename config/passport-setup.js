const passport = require('passport')
const keys = require('./keys')
const GoogleStrategy = require('passport-google-oauth20')
const User = require('../models/user-model')


passport.serializeUser((user,done)=> {
    done(null,user.id)
})


passport.deserializeUser((id, done) => { 
    User.findById(id).then((user) => { 
        done(null, user); 
    }); 
});﻿

passport.use(
        new GoogleStrategy({
            
            callbackURL: '/auth/google/redirect',
            clientID: keys.google.clientID,
            clientSecret: keys.google.clientSecret
        },
        (access,refresh,profile,done)=>{
            
            User.findOne({googleID : profile.id}).then((currUser) => {
                
                if(currUser)
                    done(null,currUser)
                
                else
                {
                    new User({
                        username : profile.displayName,
                        googleID : profile.id
                    }).save().then(user => {
                        
                        console.log(`New User Created: ${user.username}`)
                        done(null,user)
                        
                    })
      
                }
                
                
            })
            
    }
))
        
module.exports = passport
