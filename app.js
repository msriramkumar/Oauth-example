const express = require('express')
const authRoutes =  require('./routes/auth-routes')
const profileRoutes =  require('./routes/profile-routes')
const mongoose = require('mongoose')
const cookie = require('cookie-session')
const passport = require('passport')
const keys = require('./config/keys')

const app = express()

app.set('view engine','hbs')
app.use(cookie({
    maxAge :30*60*1000,
    keys:[keys.sessionkey]
}))
app.use(passport.initialize())
app.use(passport.session())
app.use('/auth',authRoutes)
app.use('/profile',profileRoutes)


mongoose.Promise =global.Promise
mongoose.connect(keys.mongoURI,{ useMongoClient: true },(err)=>{
    if(err) 
        console.log("ERROR CONNECTING TO DB!")
    else
        console.log("CONNECTED")
})

app.listen(3000,()=>{
    console.log("Listening on port 3000")
})

app.get('/',(req,res)=>{
    res.render('home',{user : req.user})
})

